const fs = require('fs')
const {parse} = require('./parser')
const html_entities = require('html-entities')
const example_files = fs.readdirSync('./plan_examples');
function cleanup(fname){
  const s0 = fs.readFileSync(`./plan_examples/${fname}`).toString()
  let s = html_entities.decode(s0)
  s = s.replace(/^[|"]|[|"]$/mg, '')
  s = s.replace(/(\r\n|\n|\r)/, '\n')
  s = s.replace(/^.*["|]?\s*(QUERY PLAN|EXPLAIN)\s*"?[\s\n]*-*[\s\n]*\n/gs, '')
  s = s.replace(/^.*EXPLAIN for:.*[\r\n]*/, '')
  s = s.replace(/([\r\n|"]*\s*)*/, '')
  s = s.replace(/^>\s*["|]/, '')
  s = s.replace(/^=\s*["|]/, '')
  s = s.replace(/\s*"?\+?-*\+?/, '')
  s = s.replace(/([\n|"]*\s*)*/, '')
  s = s.replace(/\n(\s*\n)*/g, '\n')
  if(s !== s0){
    console.log('rewrite ' + fname)
    fs.writeFileSync(`./plan_examples/${fname}`, s);
  }
  return s + '\n';
}
let passed = 0;
let failed = 0;
for(let i = 0; i < example_files.length; ++i){
  const fname = example_files[i];
  const plan_txt = cleanup(fname)
  if(plan_txt.startsWith('[SKIP]') || !plan_txt.trim()){
    continue;
  }
  try {
    parse(plan_txt)
    passed += 1
  }catch(e){
    if(e.location){
      console.log(e.message, `./plan_examples/${fname}:${e.location.start.line}:${e.location.start.column}`)
    }else{
      throw e;
    }
    failed += 1;
  }
}
console.log(`Passed: ${passed}, Failed: ${failed}`)